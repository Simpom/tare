# coding utf-8

import json
import os
import pytest
from tare.tuleap_token import TOKEN_PATH


@pytest.fixture(scope="session", autouse=True)
def remove_token(request):
    if os.path.isfile(TOKEN_PATH):
        os.remove(TOKEN_PATH)

    fake_token = {"token": "XXX",
                  "user_id": 1,
                  "uri": "tokens/123"}

    if not os.path.isdir(os.path.dirname(TOKEN_PATH)):
        os.makedirs(os.path.dirname(TOKEN_PATH))

    with open(TOKEN_PATH, 'w') as fid:
        json.dump(fake_token, fid)
