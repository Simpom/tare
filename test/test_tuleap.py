# coding utf-8

import os
import tempfile
import json
import csv
import pytest
import sys
from datetime import datetime
import tare
from tare import Tuleap, TuleapException, date_converter
try:  # Python3
    from unittest import mock
except ImportError:  # Python2
    import mock


def get_temp_file_name():
    """Generate a temporary file name.

    Use `tempfile.NamedTemporaryFile()` to create a temporary file, get its
    name, close it and return the name.

    :return: Name of a temporary file

    :Example:

    - On Windows:

    >>> get_temp_file_name()
    'C:\\Users\\USERNAME\\AppData\\Local\\Temp\\tmp15nf9mcl'
    >>> get_temp_file_name()
    'C:\\Users\\USERNAME\\AppData\\Local\\Temp\\tmp9wvsgfsx'

    - On Linux:

    >>> get_temp_file_name()
    '/tmp/tmpyweooewp'
    >>> get_temp_file_name()
    '/tmp/tmpfqsfilwe'
    """
    temp = tempfile.NamedTemporaryFile()
    temp_name = temp.name
    temp.close()
    return temp_name


def with_example(case):
    def with_example_report_from_tql(func):
        @mock.patch("getpass.getpass")
        @mock.patch("tare.tuleap_token.compatibility_input")
        def wrapper(self, compatibility_input, getpass, *args, **kwargs):
            compatibility_input.return_value = os.environ['TULEAP_USER']
            getpass.return_value = os.environ['TULEAP_PASSWORD']

            if case == 'artifact':
                artifact = self.tuleap.get_artifact(100, changeset='Status')
                return func(self, artifact)
            elif case == 'report_from_aids':
                report = self.tuleap.get_report(['101', 100, '100', 101],
                                                changeset='Status')
                return func(self, report)
            elif case == 'report_from_tql':
                requests = ["summary='HTML email notification for trackers'",
                            "summary='Platform administration'"]
                report = self.tuleap.get_report(' OR '.join(requests))
                return func(self, report)
            elif case == 'report_from_tql_and_aids':
                requests = ["summary='HTML email notification for trackers'",
                            "summary='Platform administration'"]
                report = self.tuleap.get_report(' OR '.join(requests),
                                                ['101', 100, '100', 101],
                                                changeset='Status')
                return func(self, report)

        return wrapper
    return with_example_report_from_tql


class TestTuleap():
    def setup_method(self):
        self.tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    @staticmethod
    def read_reference(reference_file):
        reference_data = ''
        full_path = os.path.join('test',
                                 'references_py{}'.format(sys.version_info[0]),
                                 reference_file)
        with open(full_path) as fid:
            reference_data = fid.read()
        return reference_data.strip()

    @with_example('artifact')
    def test_artifact_content(self, artifact):
        test_date = datetime(2011, 10, 5, 15, 34, 46)
        assert artifact['Assigned to'] == 'Gill Aron (GAron)'
        assert artifact['Type'] == 'Enhancement'
        assert artifact['Last Modified On'] == test_date
        assert artifact['Status'] == 'Open'
        assert artifact['Original Submission'] == (
            'Email notification for trackers should be in HTML'
        )
        assert artifact['Submitting organisation'] == 'ReccoMedical'
        assert artifact['Priority'] == 'Major'
        assert artifact['Submitted by'] == 'Thomas Iona (TIona)'
        assert artifact['Stage'] == 'Workaround'
        assert artifact['Summary'] == 'HTML email notification for trackers'
        assert artifact['Version'] == '4.0.18'
        assert artifact['Cross references'] == ''
        assert artifact['Category'] == 'Tracker'
        assert artifact['Submitted on'] == test_date
        assert artifact['Artifact ID'] == 100

    @with_example('artifact')
    def test_artifact_repr(self, artifact):
        assert repr(artifact) == self.read_reference('artifact.ref')

    def test_get_artifact_exception(self):
        with pytest.raises(TuleapException) as e_info:
            self.tuleap.get_artifact('non int string')
        assert str(e_info.value).startswith('Cannot deal with argument `non '
                                            'int string` of type ')

    @with_example('artifact')
    def test_get_artifact_inexistant_changeset(self, artifact):
        self.tuleap.get_artifact(100, changeset='inexistant')

    @with_example('report_from_aids')
    def test_report_from_aids(self, report):
        # Test the length of the report and its basic content
        assert len(report) == 2
        for i in range(2):
            assert report[i]['Artifact ID'] == 100 + i

    @with_example('report_from_aids')
    def test_report_from_aids_repr(self, report):
        # Test the representation of the printed report
        assert repr(report) == self.read_reference('report.ref')

    @with_example('report_from_aids')
    def test_report_from_aids_summary(self, report):
        # Test the summary of the report
        summary = os.linesep.join(['100', '101'])
        assert report.summary('{Artifact ID}') == summary

    @with_example('report_from_aids')
    def test_report_from_aids_json_export(self, report):
        # Test the JSON export of the report
        temp_name = get_temp_file_name()
        report.export_json(temp_name)
        with open(temp_name) as fid:
            json_data = json.load(fid)
            for i, artifact in enumerate(report):
                for key, value in json_data[i].items():
                    try:
                        value = date_converter(value)
                    except Exception:
                        continue
                    assert artifact[key] == value

    @with_example('report_from_aids')
    def test_report_from_aids_csv_export(self, report):
        # Test the CSV export of the report
        temp_name = get_temp_file_name()
        report.export_csv(temp_name)
        with open(temp_name) as fid:
            csv_reader = csv.DictReader(fid, delimiter=';')
            for i, row in enumerate(csv_reader):
                for key, value in row.items():
                    if key == 'Changeset':
                        continue
                    assert str(report[i][key]) == value

    def test_report_from_aids_exception(self):
        # Test exceptions
        with pytest.raises(TuleapException) as e_info:
            self.tuleap.get_report(['101', 100, 102, 'non int string', 102])
        assert str(e_info.value).startswith('Cannot deal with argument '
                                            '`[\'101\', 100, 102, \'non int '
                                            'string\', 102]` of type ')

    def test_report_from_aids_kwargs_exception(self):
        # Test exceptions
        with pytest.raises(TuleapException) as e_info:
            self.tuleap.get_report([100], unknown=42)
        assert str(e_info.value) == ('Tuleap.get_report cannot deal with the '
                                     'following keyword arguments: unknown')

    @with_example('report_from_tql')
    def test_report_from_tql(self, report):
        # Test the length of the report and its basic content
        assert len(report) == 2
        for i in range(2):
            assert report[i]['Artifact ID'] == 100 + i

    @with_example('report_from_tql')
    def test_report_from_tql_json_export(self, report):
        # Test the JSON export of the report
        temp_name = get_temp_file_name()
        report.export_json(temp_name)
        with open(temp_name) as fid:
            json_data = json.load(fid)
            for i, artifact in enumerate(report):
                for key, value in json_data[i].items():
                    try:
                        value = date_converter(value)
                    except Exception:
                        continue
                    assert artifact[key] == value

    @with_example('report_from_tql')
    def test_report_from_tql_csv_export(self, report):
        # Test the CSV export of the report
        temp_name = get_temp_file_name()
        report.export_csv(temp_name)
        with open(temp_name) as fid:
            csv_reader = csv.DictReader(fid, delimiter=';')
            for i, row in enumerate(csv_reader):
                for key, value in row.items():
                    assert str(report[i][key]) == value

    @with_example('report_from_tql_and_aids')
    def test_report_from_tql_and_aids(self, report):
        # Test the length of the report and its basic content
        assert len(report) == 2
        for i in range(2):
            assert report[i]['Artifact ID'] == 100 + i
