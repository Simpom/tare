tuleap module
=============

.. currentmodule:: tare.tuleap

TuleapException class
---------------------

.. autoclass:: TuleapException


Tuleap class
------------

.. autoclass:: Tuleap

-------------------------------------------------------------------------------

Public methods
..............

.. automethod:: Tuleap.__init__
.. automethod:: Tuleap.get_json_response
.. automethod:: Tuleap.get_artifact
.. automethod:: Tuleap.get_report
.. automethod:: Tuleap._get_artifacts_from_aids
.. automethod:: Tuleap._get_artifacts_from_tql_query
