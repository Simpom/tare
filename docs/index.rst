Welcome to **Tuleap Artifacts REporter**'s documentation!
=========================================================

.. include:: ../README.rst


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   tare
   examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
