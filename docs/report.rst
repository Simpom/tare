report module
=============

.. currentmodule:: tare.report

Report class
------------

.. autoclass:: tare.report.Report

-------------------------------------------------------------------------------

Public methods
..............

.. automethod:: tare.report.Report.__init__
.. automethod:: tare.report.Report.append
.. automethod:: tare.report.Report.__repr__
.. automethod:: tare.report.Report.__len__
.. automethod:: tare.report.Report.__getitem__
.. automethod:: tare.report.Report.__setitem__
.. automethod:: tare.report.Report.__contains__
.. automethod:: tare.report.Report.sort
.. automethod:: tare.report.Report.export_json
.. automethod:: tare.report.Report.export_csv
.. automethod:: tare.report.Report.summary
