Examples
========

Getting a single artifact data
------------------------------

Example of getting the artifact #100::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a single artifact and print its data
    artifact = tuleap.get_artifact(100)
    print(artifact)


And below is the output::

    {   'Artifact ID': 100,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Tracker',
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Original Submission': 'Email notification for trackers should be in HTML',
        'Priority': 'Major',
        'Stage': 'Workaround',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'HTML email notification for trackers',
        'Type': 'Enhancement',
        'Version': '4.0.18'}

Getting a single artifact data with a changeset
-----------------------------------------------

Example of getting the artifact #100 and its 'Status' changeset::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a single artifact and print its data
    artifact = tuleap.get_artifact(100, changeset='Status')
    print(artifact)


And below is the output::

    {   'Artifact ID': 100,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Tracker',
        'Changeset': [(datetime.datetime(2011, 10, 5, 15, 34, 46), 'Open')],
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46),
        'Original Submission': 'Email notification for trackers should be in HTML',
        'Priority': 'Major',
        'Stage': 'Workaround',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'HTML email notification for trackers',
        'Type': 'Enhancement',
        'Version': '4.0.18'}

Getting a report from a list of artifacts IDs
---------------------------------------------

Example of getting a report for the artifacts #100 and #101::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    report = tuleap.get_report([100, 101])
    print(report)

    # Export report to a csv file
    report.export_csv('my_report.csv')

    # Export report to a json file
    report.export_json('my_report.json')


And below is the output::

    [   {   'Artifact ID': 100,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Tracker',
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Original Submission': 'Email notification for trackers should be in HTML',
        'Priority': 'Major',
        'Stage': 'Workaround',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'HTML email notification for trackers',
        'Type': 'Enhancement',
        'Version': '4.0.18'},
        {   'Artifact ID': 101,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Site admin',
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Original Submission': 'I need help in my information system '
                               'administration: seems to be erros with LDAP and '
                               'others',
        'Priority': 'Medium',
        'Stage': 'New',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'Platform administration',
        'Type': 'Assistance',
        'Version': '4.0.18'}]

Getting a report from a list of artifacts IDs with a changeset
--------------------------------------------------------------

Example of getting a report for the artifacts #100 and #101 and their 'Status'
changeset::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    report = tuleap.get_report([100, 101], changeset='Status')
    print(report)

And below is the output::

    [   {   'Artifact ID': 100,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Tracker',
        'Changeset': [(datetime.datetime(2011, 10, 5, 15, 34, 46), 'Open')],
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46),
        'Original Submission': 'Email notification for trackers should be in HTML',
        'Priority': 'Major',
        'Stage': 'Workaround',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'HTML email notification for trackers',
        'Type': 'Enhancement',
        'Version': '4.0.18'},
        {   'Artifact ID': 101,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Site admin',
        'Changeset': [(datetime.datetime(2011, 10, 5, 15, 34, 46), 'Open')],
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46),
        'Original Submission': 'I need help in my information system '
                               'administration: seems to be erros with LDAP and '
                               'others',
        'Priority': 'Medium',
        'Stage': 'New',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'Platform administration',
        'Type': 'Assistance',
        'Version': '4.0.18'}]

Getting a report from a list of artifacts and print its summary
---------------------------------------------------------------

Example of getting a report for the artifacts #100 and #101 and print its
default summary::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    report = tuleap.get_report([100, 101])
    print(report.summary())

And below is the output::

    100 - HTML email notification for trackers
    101 - Platform administration

It is possible to customize the summary content and format by specifying an
argument to the summary method::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    report = tuleap.get_report([100, 101])
    print(report.summary('{Artifact ID:4} [{Status:^10}] {Summary}'))

And below is the output::

     100 [   Open   ] HTML email notification for trackers
     101 [   Open   ] Platform administration

Getting a report from a TQL request
-----------------------------------

Example of getting a report for the artifacts whose summary is 'HTML email
notification for trackers' or 'Platform administration'::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    requests = ["summary='HTML email notification for trackers'",
                "summary='Platform administration'"]
    report = tuleap.get_report(' OR '.join(requests))
    print(report.summary())

And below is the output::

    100 - HTML email notification for trackers
    101 - Platform administration

Note it is also possible to get a changeset::

    report = tuleap.get_report(' OR '.join(requests), changeset='Status')

Getting a report from a combination of TQL requests and lists of artifact IDs
-----------------------------------------------------------------------------

Example of getting a report for the artifacts whose summary is 'HTML email
notification for trackers' or 'Platform administration', or whose IDs are 99 or
from 102 to 105::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    requests = ["summary='HTML email notification for trackers'",
                "summary='Platform administration'"]
    report = tuleap.get_report([99], ' OR '.join(requests), range(102,106))
    print(report.summary('{Artifact ID:4} | {Status:^6} | {Priority:8} | {Summary}'))

And below is the output::

      99 |  Open  | Serious  | Percentages in Evolution Rate column not correct
     100 |  Open  | Major    | HTML email notification for trackers
     101 |  Open  | Medium   | Platform administration
     102 |  Open  | Serious  | Hello World only works in console
     103 | Closed | Serious  | Wrong values for one field in the tracker report
     104 | Closed | Minor    | Git: Fine grain permissions on repositories
     105 |  Open  | Critical | Make global text search more visible in docman
