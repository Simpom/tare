artifact module
===============

.. currentmodule:: tare.artifact

Artifact class
--------------

.. autoclass:: Artifact

-------------------------------------------------------------------------------

Public methods
..............

.. automethod:: Artifact.__init__
.. automethod:: Artifact.__repr__
.. automethod:: Artifact.get_keys
.. automethod:: Artifact.__getitem__
.. automethod:: Artifact.to_json

-------------------------------------------------------------------------------

Protected methods
.................

.. automethod:: Artifact._process_raw_data
.. automethod:: Artifact._get_changeset
