tuleap\_token module
====================

.. currentmodule:: tare.tuleap_token

TuleapToken class
-----------------

.. autoclass:: TuleapToken

-------------------------------------------------------------------------------

Public methods
..............

.. automethod:: TuleapToken.__init__
.. automethod:: TuleapToken.update
.. automethod:: TuleapToken.xauth_from_json
