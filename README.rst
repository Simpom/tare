.. |pipeline| image:: https://gitlab.com/Simpom/tare/badges/master/pipeline.svg

.. |coverage| image:: https://gitlab.com/Simpom/tare/badges/master/coverage.svg

=================================
 tare - Tuleap Artifact REporter
=================================

**tare** is a Python package made to easily extract data about artifacts from a
Tuleap (https://www.tuleap.org) tracker.

.. note::
    To see user documentation, please see:
    http://tare.readthedocs.io/en/master/

Non regression tests status: |pipeline| - |coverage|


Getting started
---------------

Prerequisites
.............

This module has been designed to need only default Python modules.

Getting the sources
...................

Start by getting a working copy of **tare**. You can either clone the git
repository by the following command::

   > git clone https://gitlab.com/Simpom/tare.git

Or download and extract one of the following archives:

    - https://gitlab.com/Simpom/tare/repository/master/archive.zip
    - https://gitlab.com/Simpom/tare/repository/master/archive.tar.gz
    - https://gitlab.com/Simpom/tare/repository/master/archive.tar.bz2
    - https://gitlab.com/Simpom/tare/repository/master/archive.tar

Installing
..........

1. With pip

If you got **tare** from git, install it by using::

    > pip install /path/to/tare

Or, in order to install only for current user::

    > pip install /path/to/tare --user

If you got **tare** from an archive, install it by using::

    > pip install /path/to/tare/archive.zip

Or, in order to install only for current user::

    > pip install /path/to/tare/archive.zip --user


If you install **tare** only for current user, do not forget to define its path
in the PYTHONPATH environment variable.

2. With Python setuptools::

    > python setup.py install
    > python setup.py install --prefix=/installation/path

3. Without installation

Just extract the archive and append the path of **tare** to the PYTHONPATH
environment variable.

Examples of use
---------------

Getting a single artifact data
..............................

Example of getting the artifact #100::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a single artifact and print its data
    artifact = tuleap.get_artifact(100)
    print(artifact)


And below is the output::

    {   'Artifact ID': 100,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Tracker',
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Original Submission': 'Email notification for trackers should be in HTML',
        'Priority': 'Major',
        'Stage': 'Workaround',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'HTML email notification for trackers',
        'Type': 'Enhancement',
        'Version': '4.0.18'}

Getting a report from a list of artifacts IDs
.............................................

Example of getting a report for the artifacts #100 and #101::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    report = tuleap.get_report([100, 101])
    print(report)

    # Export report to a csv file
    report.export_csv('my_report.csv')


And below is the output::

    [   {   'Artifact ID': 100,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Tracker',
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Original Submission': 'Email notification for trackers should be in HTML',
        'Priority': 'Major',
        'Stage': 'Workaround',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'HTML email notification for trackers',
        'Type': 'Enhancement',
        'Version': '4.0.18'},
        {   'Artifact ID': 101,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Site admin',
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Original Submission': 'I need help in my information system '
                               'administration: seems to be erros with LDAP and '
                               'others',
        'Priority': 'Medium',
        'Stage': 'New',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'Platform administration',
        'Type': 'Assistance',
        'Version': '4.0.18'}]


Getting a report from a TQL request
...................................

Example of getting a report for the artifacts whose summary is 'HTML email
notification for trackers'::

    from tare import Tuleap
    # Create Tuleap connection
    tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')

    # Get a report
    report = tuleap.get_report("summary='HTML email notification for trackers'")
    print(report)

    # Export report to a json file
    report.export_json('my_report.json')

And below is the output::

    [   {   'Artifact ID': 100,
        'Assigned to': 'Gill Aron (GAron)',
        'Category': 'Tracker',
        'Cross references': '',
        'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Original Submission': 'Email notification for trackers should be in HTML',
        'Priority': 'Major',
        'Stage': 'Workaround',
        'Status': 'Open',
        'Submitted by': 'Thomas Iona (TIona)',
        'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
        'Submitting organisation': 'ReccoMedical',
        'Summary': 'HTML email notification for trackers',
        'Type': 'Enhancement',
        'Version': '4.0.18'}]

Reporting error, ask for developments
-------------------------------------

In order to contact the tare developers to report an error or ask for the
development of a new functionality, please send an e-mail to incoming+Simpom/tare@gitlab.com

Author
------

- Simon Pomarède - creation of the tool - https://gitlab.com/Simpom
