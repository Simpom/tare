=========
Changelog
=========

2.0
---

- Porting to Python 2.7; work on 2.7, 3.4, 3.5 and 3.6
- Addition of functionality of creating reports from multiple requests or lists
  of artifact IDs
- setup.py automatically tested
- Documentation improvements

1.4
---

- Continuous integration deployed on the project

1.3
---

- Addition of the 'changeset' feature to the report

1.2
---

- Hotfix: setup.py updated

1.1
---

- Addition of a 'summary' method in the Report class

1.0
---

First working version.
