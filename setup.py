#!/usr/bin/env python3

import os
from setuptools import setup, find_packages
import tare


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


def data_files():
    datadir = os.path.join('docs', '_build', 'html')
    destdir = os.path.join('share', 'tare', 'doc')
    return [(d.replace(datadir, destdir), [os.path.join(d, f) for f in files])
            for d, folders, files in os.walk(datadir)]

setup(
    name='tare',
    version=tare.__version__,
    license='GPLv3',
    author='Simpom',
    description=('tare is a Python package made to easily extract data about '
                 'artifacts from a Tuleap tracker.'),
    long_description=read('README.rst'),
    packages=find_packages(exclude=['docs']),
    data_files=data_files(),
    include_package_data=True,
)
