#!/usr/bin/env python3
# coding utf-8

import os
from contextlib import closing
try:  # pragma: no cover
    from urllib.parse import urlencode
    from urllib.request import urlopen, Request
except ImportError:  # pragma: no cover
    from urllib import urlencode
    from urllib2 import urlopen, Request
import getpass
import json


TOKEN_PATH = os.path.join(os.path.expanduser('~'),
                          '.local',
                          'share',
                          'keyrings',
                          'tuleap_keyring.json')


def compatibility_input(s=''):  # pragma: no cover
    try:
        return raw_input(s)
    except NameError:
        return input(s)


class TuleapToken:
    """Class to manage a token to Tuleap API.

    .. warning::

        This class is not supposed to be instantiated nor manipulated by the
        user. No object of this type are to be created by the user.

    :Methods:
        - Public:
            - :func:`~__init__`
            - :func:`~update`
            - :func:`~xauth_from_json`
    """
    def __init__(self, bugtracker_url):
        """Create a Tuleap token manager.

        The `authentication` attribute is a dictionary that contains the token
        data to be passed to the Tuleap API. It is empty by default, ie when no
        token is created yet.

        :param bugtracker_url: URL of the Tulep bugtracker
        """
        self.bugtracker_url = bugtracker_url
        # Default values
        self.authentication = {}

        # If the token file exist try and store its content
        if os.path.isfile(TOKEN_PATH):
            with open(TOKEN_PATH) as json_file:
                try:
                    json_content = json.load(json_file)
                    self.authentication = self.xauth_from_json(json_content)
                except ValueError as _:  # pragma: no cover
                    pass

    def update(self):
        """Update the `authentication` attribute by prompting the user for
        username and password. If correct values are entered, token values are
        stored into the `TOKEN_PATH` file.
        """
        print("Connection to {}".format(self.bugtracker_url))

        # Prompt for username and password
        username = compatibility_input("Login: ")
        password = getpass.getpass()
        authentication = {"username": username, "password": password}

        # Ask Tuleap for token
        auth_data = urlencode(authentication)
        token_request = Request(self.bugtracker_url+"tokens",
                                               auth_data.encode('utf-8'))

        with closing(urlopen(token_request)) as response:
            # Check for folder existence
            if not os.path.isdir(os.path.dirname(TOKEN_PATH)):
                os.makedirs(os.path.dirname(TOKEN_PATH))  # pragma: no cover

            with open(TOKEN_PATH, mode='w') as json_file:
                json_response = response.read().decode('utf-8')
                json_content = json.loads(json_response)
                self.authentication = self.xauth_from_json(json_content)
                json.dump(json_content, json_file)

    @staticmethod
    def xauth_from_json(json_content):
        """Create the "X-Auth" dictionary from the Tuleap API JSON response.

        :param json_content: Tuleap API JSON response
        :return: "X-Auth" dictionary
        """
        return {
            "X-Auth-UserId": str(json_content["user_id"]),
            "X-Auth-Token": json_content["token"]
        }
