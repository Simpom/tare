#!/usr/bin/env python3
# coding utf-8

import pprint
from collections import defaultdict
import tare


class Artifact:
    """Class to represent a Tuleap artifact.

    An :class:`Artifact` is created by passing the JSON data get from the
    Tuleap API. These data are parsed in order to get as much data as possible,
    by applying converters to each and every element of the JSON got.

    Each created artifact has two attributes:

    - the raw JSON data got from Tuleap (stored so as to be able and check its
      content)
    - the converted data as a dictionary associating the field names to the
      converted value

    .. note::

        Converters were created from the following Tuleap JavaScript source file:

        https://github.com/Enalean/tuleap/blob/a55b3be976d0c3cf1fa2b426eae094cbdffd76f4/plugins/agiledashboard/www/js/kanban/src/app/in-properties-filter/in-properties-filter.js

    .. warning::

        This class is not supposed to be instantiated by the user. Objects of
        :class:`Artifact` type are created by the
        :func:`~tare.tuleap.Tuleap.get_artifact` method of the
        :class:`~tare.tuleap.Tuleap` class.

        Examples given in the current page are created from a JSON sample.

    :Methods:
        - Public:
            - :func:`~__init__`
            - :func:`~__repr__`
            - :func:`~get_keys`
            - :func:`~__getitem__`
            - :func:`~to_json`
        - Protected:
            - :func:`~_process_raw_data`
            - :func:`~_get_changeset`
    """

    # -------------------------------------------------------------------------
    # Definition of the converters functions used to parse JSON raw data from
    # Tuleap into meaningful data.
    def display_label_converter(x):
        """Converter for the 'display_name' or 'label' elements, got when field
        type is one of 'sb', 'rb', 'cb', 'tbl', 'msb', 'shared'.
        """
        try:
            value = x['values'][0]
            if 'display_name' in value:
                return value['display_name']
            else:
                return value['label']
        except IndexError:  # pragma: no cover
            return u''

    # List of tuples that associate a list of 'type' extracted from the JSON
    # object to a function used to produce meaningful data
    converters_def = [
        (['sb', 'rb', 'cb', 'tbl', 'msb', 'shared'],
         display_label_converter),
        (['string',  'text', 'int', 'float', 'aid', 'atid', 'priority'],
         lambda x: x['value']),
        (['file'],
         lambda x: ','.join([f['name'] for f in x['file_descriptions']])),
        (['cross'],
         lambda x: ','.join([elem['ref'] for elem in x['value']])),
        (['perm'],
         lambda x: x['group']),
        (['subby', 'lubby'],
         lambda x: x['value']['display_name']),
        (['date', 'lud', 'subon'],
         # lambda x: '' if not x['value'] else x['value']),
        lambda x: u'' if not x['value'] else tare.date_converter(x['value'])),
        (['computed'],
         lambda x: x['manual_value'] if 'manual_value' in x else x['value']),
    ]

    # Dictionary that associate each and every 'type' to a converter function
    converters = defaultdict(lambda: lambda x: 'Converter not implemented')
    for keys, function in converters_def:
        converters.update(dict.fromkeys(keys, function))
    # -------------------------------------------------------------------------

    def __init__(self, raw_json_data, changeset=None):
        """Create an Artifact from a JSON ticket representation.

        :param json_data: JSON data get from the Tuleap API
        :type json_data: dict
        :param changeset: Name of the field to get the changeset, None if no
                          no changeset wanted
        :type changeset: str

        >>> tuleap = Tuleap('https://tuleap.net/plugins/tracker/?tracker=9')
        >>> artifact = tuleap.get_artifact(99)
        >>> artifact = tuleap.get_artifact(99, changeset='Status')
        """

        # JSON data get from the Tuleap API
        self.json_data = None
        self._process_raw_data(raw_json_data)  # set self.json_data

        # Formatted data generated from JSON data thanks to the converters
        self.formatted_data = {}

        for field in self.json_data[-1]['values'].values():
            try:
                converter = Artifact.converters[field['type']]
                self.formatted_data[field['label']] = converter(field)
            except KeyError as _:  # pragma: no cover
                continue

        # Get the changeset (if requiered)
        if changeset:
            self._get_changeset(changeset)

    def _process_raw_data(self, raw_json_data):
        """Depending on the requestand the Tuleap database state, the JSON data
        got from the Tuleap API have a different structure. This method
        processes those different structures to output a single one.

        :param raw_json_data: JSON data from Tuleap API
        :type raw_json_data: list or dict
        """
        # Structure must be a list of artifact states
        if isinstance(raw_json_data, list):
            self.json_data = raw_json_data
        else:  # raw_json_data is a dict
            self.json_data = [raw_json_data]

        # For each artifact state, the element for 'values' key must be a
        # dictionnary associating an (integers) key to a dict value
        for i, state in enumerate(self.json_data):
            values = state['values']
            if isinstance(values, list):
                values = {i:val for i, val in enumerate(values)}
            else:  # values is a dict
                values = {int(i):val for i, val in values.items()}
            self.json_data[i]['values'] = values

    def _get_changeset(self, changeset):
        """Get the changeset for the field given as `changeset` argument.

        .. note:: If a changeset if requiered, the additional key 'Changeset'
           is added to the self.formatted_data attribute

        :param changeset: name of the field a changeset is asked
        :type changeset: str
        """
        changeset_index = None
        for index, field in self.json_data[-1]['values'].items():
            if field['label']==changeset:
                changeset_index = index
                converter = Artifact.converters[field['type']]
                break

        if not changeset_index:
            return

        self.formatted_data[u'Changeset'] = []
        last_value = None

        for event in self.json_data:
            value = converter(event['values'][changeset_index])
            if value != last_value:
                last_modified_date = event['last_modified_date']
                change_date = tare.date_converter(last_modified_date)
                self.formatted_data['Changeset'].append((change_date, value))
                last_value = value

    def __repr__(self):
        """Method used for getting the string representation of the object

        :return: string representation of the object

        >>> artifact = tuleap.get_artifact(99)
        >>> artifact
        {   'Artifact ID': 99,
            'Assigned to': 'Gill Aron (GAron)',
            'Category': 'Site admin',
            'Cross references': '',
            'Last Modified On': datetime.datetime(2017, 9, 7, 16, 54, 23, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Original Submission': 'The percentage in the column "Evolution Rate '
                                   '(%)" of the plugin "disk usage per project" is '
                                   'not correct.\n'
                                   'Can you set it well in order to display correct '
                                   'percentages, please?',
            'Priority': 'Serious',
            'Stage': 'New',
            'Status': 'Open',
            'Submitted by': 'Thomas Iona (TIona)',
            'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Submitting organisation': 'AzzuliSAS',
            'Summary': 'Percentages in Evolution Rate column not correct',
            'Type': 'Bug fix',
            'Version': '4.0.18'}
            
        """
        return pprint.pformat(self.formatted_data, indent=4)

    def get_keys(self):
        """Get the list of all the available keys of the artifact.

        :return: list of the available keys

        >>> artifact = tuleap.get_artifact(99)
        >>> artifact.get_keys()
        dict_keys(['Submitted on', 'Last Modified On', 'Artifact ID', 'Cross references', 'Summary', 'Stage', 'Category', 'Priority', 'Submitted by', 'Submitting organisation', 'Type', 'Version', 'Assigned to', 'Status', 'Original Submission'])
        """
        return self.formatted_data.keys()

    def __getitem__(self, key):
        """Access the `key` element of the artifact.

        If `key` does not exist in the current artifact, the empty string is
        returned.

        :return: value extracted for the `key` element

        >>> artifact = tuleap.get_artifact(99)
        >>> artifact['Priority']
        'Serious'
        >>> artifact['Inexistant key']
        ''

        """
        return self.formatted_data.get(key, '')

    def to_json(self):
        """Export the artifact extracted data to a JSON structure.

        :return: json representation of the artifact

        >>> artifact = tuleap.get_artifact(99)
        >>> artifact.to_json()
        {'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))), 'Last Modified On': datetime.datetime(2017, 9, 7, 16, 54, 23, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))), 'Artifact ID': 99, 'Cross references': '', 'Summary': 'Percentages in Evolution Rate column not correct', 'Stage': 'New', 'Category': 'Site admin', 'Priority': 'Serious', 'Submitted by': 'Thomas Iona (TIona)', 'Submitting organisation': 'AzzuliSAS', 'Type': 'Bug fix', 'Version': '4.0.18', 'Assigned to': 'Gill Aron (GAron)', 'Status': 'Open', 'Original Submission': 'The percentage in the column "Evolution Rate (%)" of the plugin "disk usage per project" is not correct.\nCan you set it well in order to display correct percentages, please?'}

        """
        return self.formatted_data
