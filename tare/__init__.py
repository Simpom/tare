# coding utf-8


from datetime import datetime
from .tuleap import Tuleap, TuleapException


__version__ = '2.0'


def date_converter(tuleap_datetime):
    """Convert Tuleap datetime string into Python standard datetime

    >>> date_converter('2017-03-09T14:56:30+01:00')
    datetime.datetime(2017, 3, 9, 14, 56, 30, tzinfo=datetime.timezone(datetime.timedelta(0, 3600)))
    """  # nopep8
    python_str_datetime = tuleap_datetime[:19]
    return datetime.strptime(python_str_datetime, '%Y-%m-%dT%H:%M:%S')
