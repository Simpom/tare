#!/usr/bin/env python3
# coding utf-8

import os
import json
import csv
import pprint
from itertools import chain
from datetime import date, datetime


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code.

    Actual serializer additions:

    - datetime
    - date
    """
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))  # pragma: no cover


class Report:
    """Class to represent a Tuleap report.

    A :class:`~tare.report.Report` is an aggregation of
    :class:`~tare.artifact.Artifact`.

    A :class:`~tare.report.Report` is created by passing the JSON data get from
    the Tuleap API. For each and every element of these data, an
    :class:`~tare.artifact.Artifact` is created and stored in a local
    list.

    .. warning::

        This class is not supposed to be instantiated by the user. Objects of
        :class:`~tare.report.Report` type are created by the
        :func:`~tare.tuleap.Tuleap.get_report` method of the
        :class:`~tare.tuleap.Tuleap` class.

        Examples given in the current page are created from a JSON sample.

    :Methods:
        - Public:
            - :func:`~tare.report.Report.__init__`
            - :func:`~tare.report.Report.append`
            - :func:`~tare.report.Report.__repr__`
            - :func:`~tare.report.Report.__len__`
            - :func:`~tare.report.Report.__getitem__`
            - :func:`~tare.report.Report.__setitem__`
            - :func:`~tare.report.Report.export_csv`
            - :func:`~tare.report.Report.summary`
    """
    def __init__(self):
        """Create a Report.

        >>> report = Report()
        """
        self.artifacts = []

    def append(self, artifact):
        """Append an Artifact to the current report.

        :param artifact: artifact to append to the Report
        :type artifact: :class:`~tare.artifact.Artifact`

        >>> artifact
        {   'Artifact ID': 100,
            'Assigned to': 'Gill Aron (GAron)',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Priority': 'Major'}
        >>> report.append(artifact)
        """
        self.artifacts.append(artifact)

    def __repr__(self):
        """Method used for getting the string representation of the object

        :return: string representation of the object

        >>> report
        [   {   'Artifact ID': 100,
            'Assigned to': 'Gill Aron (GAron)',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Priority': 'Major'}]
        """
        return pprint.pformat(self.artifacts, indent=4)

    def __len__(self):
        """Get the number of artifacts in the current report

        :return: int number of elements of the report

        >>> len(report)
        1
        """
        return len(self.artifacts)

    def __getitem__(self, index):
        """Access the index-th artifact of the report.

        If `index` is greater than the number of artifacts, an IndexError will
        be thrown.

        :param index: Index of the artifact to get
        :return: index-th artifact of the report

        >>> report[0]
        {   'Artifact ID': 100,
            'Assigned to': 'Gill Aron (GAron)',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Priority': 'Major'}
        >>> report[1]
        IndexError: list index out of range
        """
        return self.artifacts[index]

    def __setitem__(self, index, value):
        """Assign the index-th artifact of the report to value.
        """
        self.artifacts[index] = value

    def __contains__(self, aid):
        """Test if aif is one of the artifact IDs of the report.

        :param aid: Artifact ID to test
        :return: True if an artifact of ID aid exists in the report
        """
        return aid in [artifact['Artifact ID'] for artifact in self.artifacts]

    def sort(self):
        """In place sort the artifacts by IDs.
        """
        self.artifacts = sorted(self.artifacts, key=lambda a: a['Artifact ID'])

    def export_json(self, filename):
        """Export the current report to a JSON file.

        :param filename: name of the json file to produce
        :type filename: str

        >>> report.export_json('my_report.json')
        >>> import json
        >>> with open('my_report.json') as fid:
        ...     json.load(fid)
        ...
        [{'Artifact ID': 100, 'Priority': 'Major', 'Last Modified On': '2011-10-05T15:34:46+02:00', 'Assigned to': 'Gill Aron (GAron)'}]
        """
        json_repr = []
        for artifact in self.artifacts:
            json_repr.append(artifact.to_json())
        with open(filename, 'w') as fid:
            json.dump(json_repr, fid, indent=4,
                      ensure_ascii=False, default=json_serial)

    def export_csv(self, filename):
        """Export the current report to a CSV file.

        :param filename: name of the CSV file to produce
        :type filename: str

        >>> report.export_csv('my_report.csv')
        >>> import csv
        >>> with open('my_report.csv') as fid:
        ...     reader = csv.reader(fid, delimiter=';')
        ...     for row in reader:
        ...         print(', '.join(row))
        ...
        Artifact ID, Assigned to, Last Modified On, Priority
        100, Gill Aron (GAron), 2011-10-05 15:34:46+02:00, Major
        """
        # Get all the fields that exist at least once in the artifact data
        columns = list(set(list(chain.from_iterable(artifact.get_keys()
                                for artifact in self.artifacts))))
        columns.sort()
        changeset_index = None
        if 'Changeset' in columns:
            changeset_index = columns.index('Changeset')

        flatten_repr = [columns]
        for artifact in self.artifacts:
            row = [artifact[col] for col in columns]
            if changeset_index:
                row2 = '\r\n'.join(['{} - {}'.format(date, status)
                                    for date, status in row[changeset_index]])
                row[changeset_index] = row2
            flatten_repr.append(row)

        with open(filename, 'w') as fid:
            writer = csv.writer(fid, delimiter=';')
            writer.writerows(flatten_repr)

    def summary(self, formatting_string='{Artifact ID} - {Summary}'):
        """Return a string containing a summary of the report.

        The summary contains a line per artifact.
        By default, each line contains the artifact ID and its summary.
        This behaviour can be modified by passing this method a string that
        defines the format to print.

        >>> report.summary()
        '100 - HTML email notification for trackers'

        >>> report.summary('{Artifact ID} - {Summary}')
        '100 - HTML email notification for trackers'

        >>> report.summary('{Artifact ID} [{Priority}] {Summary}')
        '100 [Major] HTML email notification for trackers'
        """
        output = []
        for artifact in self.artifacts:
            output.append(formatting_string.format(**artifact.to_json()))
        return os.linesep.join(output)
