#!/usr/bin/env python3
# coding utf-8

try:  # pragma: no cover
    from urllib.parse import quote
    from urllib.request import urlopen, Request
    from urllib.error import HTTPError
except ImportError:  # pragma: no cover
    from urllib import quote
    from urllib2 import urlopen, Request, HTTPError

try:  # pragma: no cover
    from itertools import zip_longest
except ImportError:  # pragma: no cover
    from itertools import izip_longest as zip_longest

import json
from functools import wraps
import ssl

from .tuleap_token import TuleapToken
from .artifact import Artifact
from .report import Report


class TuleapException(Exception):
    """Base exception class for exceptions thrown by the `Tuleap` class.
    """
    pass


def with_token(func):
    """Decorator that enables to call a method **of Tuleap class** with token
    set.
    Run as follows: try and run `func` once; if it fails, update the token
    and run `func` a second time.

    :param func: function to decorate
    :return: decorated function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except HTTPError as err:
            if err.code == 401:
                self = args[0]
                self.token.update()
                return func(*args, **kwargs)
            else:
                raise err  # pragma: no cover
    return wrapper


class Tuleap:
    """Class to manage Tuleap connexions and question its database.

    A :class:`Tuleap` object is instantiated by passing a Tuleap bugtracker API
    URL and a tracker ID.

    :Methods:
        - Public:
            - :func:`~__init__`
            - :func:`~get_json_response`
            - :func:`~get_artifact`
            - :func:`~get_report`
        - Protected:
            - :func:`~_get_artifacts_from_aids`
            - :func:`~_get_artifacts_from_tql_query`
    """
    def __init__(self, url):
        """Create a Tuleap connexion from an URL.

        :param url: full URL of the Tuleap bugtracker
        :type bugtracker_url: str

        >>> tuleap = Tuleap('http://tuleap.net/plugins/tracker/?tracker=9')
        """
        self.bugtracker_url = url[:url.find('/plugins/tracker/')] + '/api/'
        self.trackers_id = url[url.find('?tracker=')+9:]
        self.token = TuleapToken(self.bugtracker_url)

    @with_token
    def get_json_response(self, partial_url):
        """Get a JSON response from a relative URL

        :param partial_url: part of the URL to prompt the API
        :type partial_url: str
        :return: JSON representation of the Tuleap API response
        """
        full_url = self.bugtracker_url + partial_url
        request = Request(full_url)
        for key, value in self.token.authentication.items():
            request.add_header(key, value)
        context = ssl._create_unverified_context()
        response = urlopen(request, context=context)
        return json.loads(response.read().decode('utf-8'))

    def get_artifact(self, aid, changeset=None):
        """Get a single artifact from its Artifact ID

        :param aid: ID of the artifact
        :type aid: str or int
        :param changeset: True in order to get the changeset, False otherwise
        :type changeset: bool
        :return: :class:`Artifact`

        >>> artifact = tuleap.get_artifact(100)
        >>> type(artifact)
        <class 'tare.artifact.Artifact'>
        >>> artifact
        {   'Artifact ID': 100,
            'Assigned to': 'Gill Aron (GAron)',
            'Category': 'Tracker',
            'Cross references': '',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Original Submission': 'Email notification for trackers should be in HTML',
            'Priority': 'Major',
            'Stage': 'Workaround',
            'Status': 'Open',
            'Submitted by': 'Thomas Iona (TIona)',
            'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Submitting organisation': 'ReccoMedical',
            'Summary': 'HTML email notification for trackers',
            'Type': 'Enhancement',
            'Version': '4.0.18'}
        >>> artifact = tuleap.get_artifact(99, changeset='Status')
        >>> artifact
        {   'Artifact ID': 99,
            'Assigned to': 'Gill Aron (GAron)',
            'Category': 'Site admin',
            'Changeset': [   (   datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
                                 'Open')],
            'Cross references': '',
            'Last Modified On': datetime.datetime(2017, 9, 7, 16, 54, 23, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Original Submission': 'The percentage in the column "Evolution Rate '
                                   '(%)" of the plugin "disk usage per project" is '
                                   'not correct.\n'
                                   'Can you set it well in order to display correct '
                                   'percentages, please?',
            'Priority': 'Serious',
            'Stage': 'New',
            'Status': 'Open',
            'Submitted by': 'Thomas Iona (TIona)',
            'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Submitting organisation': 'AzzuliSAS',
            'Summary': 'Percentages in Evolution Rate column not correct',
            'Type': 'Bug fix',
            'Version': '4.0.18'}
        """
        try:
            aid = int(aid)
        except (TypeError, ValueError):  # other types
            msg = 'Cannot deal with argument `{arg}` of type `{type}`'
            raise TuleapException(msg.format(arg=aid, type=type(aid)))

        partial_url = 'artifacts/{aid}'.format(aid=aid)
        if changeset:
            partial_url += '/changesets?limit=1000'
        json_response = self.get_json_response(partial_url)
        return Artifact(json_response, changeset=changeset)

    def get_report(self, *args, **kwargs):
        """Get a report (aggregation of artifacts), either from a list of
        Artifact IDs, or from a Tuleap Query Language query.

        If `arg` is a list, it has to be a list of AIDs.
        If `arg` is a string, it is supposed to be a TQL query.
        Else an exception is raised.

        :param arg: list of AIDs, or TQL query
        :type aid: list of ints, or str

        :return: :class:`Report`

        >>> report = tuleap.get_report([100, 101])
        >>> type(report)
        <class 'tare.report.Report'>
        >>> report
        [   {   'Artifact ID': 100,
            'Assigned to': 'Gill Aron (GAron)',
            'Category': 'Tracker',
            'Cross references': '',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Original Submission': 'Email notification for trackers should be in HTML',
            'Priority': 'Major',
            'Stage': 'Workaround',
            'Status': 'Open',
            'Submitted by': 'Thomas Iona (TIona)',
            'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Submitting organisation': 'ReccoMedical',
            'Summary': 'HTML email notification for trackers',
            'Type': 'Enhancement',
            'Version': '4.0.18'},
            {   'Artifact ID': 101,
            'Assigned to': 'Gill Aron (GAron)',
            'Category': 'Site admin',
            'Cross references': '',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Original Submission': 'I need help in my information system '
                                   'administration: seems to be erros with LDAP and '
                                   'others',
            'Priority': 'Medium',
            'Stage': 'New',
            'Status': 'Open',
            'Submitted by': 'Thomas Iona (TIona)',
            'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Submitting organisation': 'ReccoMedical',
            'Summary': 'Platform administration',
            'Type': 'Assistance',
            'Version': '4.0.18'}]

        >>> report = tuleap.get_report([100, 101], changeset='Status')
        >>> report
        [   {   'Artifact ID': 100,
            'Assigned to': 'Gill Aron (GAron)',
            'Category': 'Tracker',
            'Changeset': [   (   datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
                                 'Open')],
            'Cross references': '',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Original Submission': 'Email notification for trackers should be in HTML',
            'Priority': 'Major',
            'Stage': 'Workaround',
            'Status': 'Open',
            'Submitted by': 'Thomas Iona (TIona)',
            'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Submitting organisation': 'ReccoMedical',
            'Summary': 'HTML email notification for trackers',
            'Type': 'Enhancement',
            'Version': '4.0.18'},
            {   'Artifact ID': 101,
            'Assigned to': 'Gill Aron (GAron)',
            'Category': 'Site admin',
            'Changeset': [   (   datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
                                 'Open')],
            'Cross references': '',
            'Last Modified On': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Original Submission': 'I need help in my information system '
                                   'administration: seems to be erros with LDAP and '
                                   'others',
            'Priority': 'Medium',
            'Stage': 'New',
            'Status': 'Open',
            'Submitted by': 'Thomas Iona (TIona)',
            'Submitted on': datetime.datetime(2011, 10, 5, 15, 34, 46, tzinfo=datetime.timezone(datetime.timedelta(0, 7200))),
            'Submitting organisation': 'ReccoMedical',
            'Summary': 'Platform administration',
            'Type': 'Assistance',
            'Version': '4.0.18'}]

        """
        # Sort args in order to compute TQL first and then artifacts by ids
        # in order to prevent getting some artifact too many times
        sorted_args = ([arg for arg in args if type(arg) is str]
                       + [arg for arg in args if type(arg) in [list, range]])

        # Check that only `changeset` can be specified as kwargs
        changeset = kwargs.pop('changeset', None)
        if len(kwargs):
            msg = ('Tuleap.get_report cannot deal with the following keyword '
                   'arguments: {kw}'.format(kw=', '.join(kwargs.keys())))
            raise TuleapException(msg)
                
        report = Report()
        for arg in sorted_args:
            # Prepare exception in case of unparsable argument
            msg = 'Cannot deal with argument `{arg}` of type `{type}`'
            exception = TuleapException(msg.format(arg=arg, type=type(arg)))

            if type(arg) in [list, range]:
                try:
                    # try conversion to int for each element
                    arg = [int(x) for x in arg if not int(x) in report]
                    method = self._get_artifacts_from_aids
                except ValueError:
                    raise exception
            elif type(arg) is str:  # expected TQL string
                method = self._get_artifacts_from_tql_query
            else:
                raise exception  # pragma: no cover
    
            for artifact in method(arg):
                report.append(artifact)

        report.sort()
    
        if changeset:
            for i in range(len(report)):
                artifact_id = report[i]['Artifact ID']
                report[i] = self.get_artifact(artifact_id, changeset=changeset)
    
        return report

    def _get_artifacts_from_aids(self, artifacts_ids):
        """Protected method to get a report from a list of AIDs

        :param artifacts_ids: list of artifact IDS
        :param artifacts_ids: list of convertible-to-int elements
        :return: :class:`Report`
        """
        N = 100  # maximum number of artifacts the "/artifacts" can return

        # Remove duplicates
        artifacts_ids = list(set([str(int(aid)) for aid in artifacts_ids]))
        artifacts_ids.sort()

        partial_url = 'artifacts?query=%7B%22id%22%3A%5B{ids}%5D%7D'
        artifacts = []

        iterators = [iter(artifacts_ids)] * N
        for artifacts_ids_sublist in zip_longest(*iterators):
            ids = '%2C'.join(str(aid) for aid in artifacts_ids_sublist if aid)
            json_response = self.get_json_response(partial_url.format(ids=ids))

            for json_artifact in json_response.get('collection', {}):
                artifacts.append(Artifact(json_artifact))

        return artifacts

    def _get_artifacts_from_tql_query(self, tql_query):
        """Protected method to get a report from a TQL query

        :param tql_query: TQL query
        :param tql_query: str
        :return: :class:`Report`
        """
        artifacts = []
        query = quote(tql_query)
        partial_url = ('trackers/{trackers_id}/artifacts?'
                       'values=all&limit=1000&expert_query={query}')
        partial_url = partial_url.format(trackers_id=self.trackers_id,
                                         query=query)
        json_response = self.get_json_response(partial_url)
        for json_artifact in json_response:
            artifacts.append(Artifact(json_artifact))
        return artifacts
